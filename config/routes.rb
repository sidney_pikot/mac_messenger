Rails.application.routes.draw do
  devise_for :users, controllers: { 
    sessions: 'api/v1/users/sessions',
    registrations: "api/v1/users/registrations"
   }
   mount ActionCable.server => '/cable'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # root 'api/v1/threads#home'
  root :to => redirect('/threads')
  get '/threads', :to => 'api/v1/threads#home'
  namespace :api do
    namespace :v1 do 
      namespace :users,path: 'user' do
        devise_scope :user do
          post 'sign_in',         to: 'sessions#create'
          get 'sign_up',          to: 'registrations#new'
          post 'register',        to: 'registrations#create'
          delete 'sign_out',      to: 'sessions#destroy'
        end
      end
    
    # get '/threads/conversation',   to: 'threads#home'
    end
  end


  namespace :ajax do
    namespace :v1 do
      get 'thread'     => 'threads#load_threads'
      get 'search'     => 'threads#search_thread'
      post 'reply_thread'=> 'threads#reply_thread'
    end
  end
end
