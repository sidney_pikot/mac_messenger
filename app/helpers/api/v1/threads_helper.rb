module Api::V1::ThreadsHelper

    def format_thread(convo = nil,participant)
		convo = {
            thread_id:convo.nil? ? nil : convo.id,
            user_id: participant.id,
            # parti: profile(convo.participants.select{|p|p.id!=@user.id}[0],"task"),
            fullname: participant.fullname,
            image: participant.image.thumb.url,
			# with: format_user(convo),
			# receipt: convo.receipts.select{|r|r.receiver_id==@user.id}.first,
            last_message: convo.nil? ? "No conversation yet" : convo.last_message.body,
            class: convo.nil? ? participant.class : convo.class
        }
        
    end
    def format_user(user)
        user = {
            id: user.id,
            fullname: user.fullname,
            image: user.image.thumb.url
        }
    end
    def format_message(message)
        message = {
            id: message.id,
            sender: message.sender,
            sender_id: message.sender_id,
            body: message.body,
            created_at:message.created_at.strftime('%e %b') + "AT" + message.created_at.strftime('%l:%M %p'),
            attachment: message.attachment,
            icon: message.attachment.present? ? attach_icon(message.attachment.content_type) : ""
            # '/file_icons/doc.png'

        }
    end
    def attach_icon(content_type)
        puts content_type
        icon = '/file_icons/file.png'
        jpg = 'jpeg'
        png = 'png'
        plain = 'plain'
        zip = 'zip'
        text = 'text'
        excel = ['xsl', 'xls','xlt','xlm','xlsx','xlsm',"xltx",'xltm']
        doc = ['docx,''docm','dotx','dotm','docb','doc','dot']
        ppt = ['ppt','pot','pps','pptx','pptm','potx','potm','ppam','ppsx','ppsm','sldx','sldm']
        if content_type.include?(plain) || content_type.include?(text)
            icon = '/file_icons/txt.png'
        elsif content_type.include?(jpg)
            icon = '/file_icons/jpg.png'
        elsif content_type.include?(png)
            icon = '/file_icons/png.png'
        elsif content_type.include?(zip)
            icon = '/file_icons/zip.png'
        elsif doc.any?{|d| content_type.include?(d)}
            icon = '/file_icons/doc.png'
        elsif excel.any?{|d| content_type.include?(d)}
            icon = '/file_icons/xls.png'
        elsif ppt.any?{|d| content_type.include?(d)}
            icon = '/file_icons/ppt.png'
        else
            icon
        end
        return icon
    end
end
