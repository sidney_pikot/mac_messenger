$(document).on 'turbolinks:load', ->
  $ ->
    if $('#home').length
      convo_id = $('#thread_id').val()
      $('.thread.active').removeClass('active')
      $('.thread').first().addClass('active')
      # console.log(convo_id)
      keyword = $('.search').val()
      display_threads(keyword)
      if $('.thread').length
        greet()
        thread_onclick()

  $('#action_menu_btn').on 'click', ->
    $('.action_menu').toggle()

  $('.search').on 'keyup', ->
    keyword = $(this).val()
    display_threads(keyword)

  greet = (convo_id, user_id, class_) ->
    convo_id ?= -1
    user_id ?= -1
    $.ajax
      url: '/ajax/v1/thread'
      data: {
        id: convo_id,
        user_id: user_id,
        class_: class_
      }
      type: 'get'
      cache: false
      success: (data) ->
        $(".scroll").scrollTop($(".scroll")[0].scrollHeight)
        $('.type_msg').focus()
        detect_enter()
        # newurl = window.location.protocol + '//' + window.location.host + window.location.pathname + '?id=' + convo_id
        # window.history.pushState { path: newurl }, '', newurl
        return
    return#greet end

  thread_onclick = () ->
    $('.thread').on 'click', ->
      convo_id = $(this).data('thread_id')
      user_id = $(this).data('user_id')
      class_ = $(this).data('class')
      greet(convo_id, user_id, class_)
      $('.thread.active').removeClass('active')
      $(this).addClass('active')
    return

  detect_enter = () ->
    $('#thread_body').on 'keyup', (e) ->
      if e.keyCode == 13 && !e.shiftKey
        $('.send_btn.send_message').trigger('click')
    return

  display_threads = (keyword)  ->
    keyword ?= ""
    $.ajax
      url: "/ajax/v1/search"
      data: {
        keyword: keyword
      }
      type: 'get'
      cache: false
      success: (data) ->
        thread_onclick()
        return
    return

  return #end
