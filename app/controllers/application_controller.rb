class ApplicationController < ActionController::Base
    # def check_authentication
	# 	@user = User.find_by_auth_token(params[:auth_token])
	# 	if @user.present?
	# 		return @user
	# 	else
	# 		render json:{message: "Invalid user authentication token",status: 300},status:300
	# 	end
    # end
before_action :configure_permitted_parameters, if: :devise_controller?
helper_method :mailbox
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:fullname])
  end
  def mailbox
    @mailbox ||= current_user.mailbox
  end
  private
  def after_sign_in_path_for(resource)
    # case resource.class.to_s
    # # when "Driver"
    # #   redirection_path = resource.companies.present? ? edit_user_management_company_path(id: resource.companies.first.id) : listings_user_management_management_index_path(type: "ads")
    # #   request.env['omniauth.origin'] || stored_location_for(resource) || redirection_path
    # when "Admin"
    #   request.env['omniauth.origin'] || stored_location_for(resource) || dashboard_admins_pages_path
    # when "Operator"
    #   request.env['omniauth.origin'] || stored_location_for(resource) || dashboard_operators_pages_path
    # end
    c = resource.mailbox.conversations.first
    # puts c.id
    request.env['omniauth.origin'] || stored_location_for(resource) || threads_path(c)
  end
end
