class Ajax::V1::ThreadsController < ApplicationController
    respond_to :js
    # skip_before_action :verify_authenticity_token  
    before_action :get_threads, only: [:search_thread]
    before_action :get_thread, only: [:load_threads]
    include Api::V1::ThreadsHelper
    def load_threads
      @thread = Mailboxer::Message.new
      puts params
      if @conv.present? && params[:class_] == "Mailboxer::Conversation"
        @participants = []
        @messages = []

        @messages2 = @conv.messages.order(:created_at).to_ary
        @participants2 = @conv.participants

        @participants2.each do |p|
          @participants.push(format_user(p))
        end
        @messages2.each do |p|
          @messages.push(format_message(p))
        end
        found = false
        @convo_with = @participants.reject{|p|found = false if found && p[:user_id] == current_user.id}.first
        puts @convo_with
        puts @participants
        @count = @messages.count || 0
      else
        @thread
        @convo_with = format_user(@conv)
        @count = 0
      end
    end

    def search_thread
      @threads = []
      puts 
      if params[:keyword].present? 
        @users = User.filtered(params[:keyword]).result
        @users.each do |u|
          @counter = 1
          @threads_l.each do |t|
            puts "uid"
            puts t.inspect
            if t.is_participant?(u) && t.originator != u
              puts "is it true?"
              puts t.is_participant?(u)
              @threads.push(format_thread(t,u))
              @counter += 1
              break
            elsif t.is_participant?(u) && t.originator == u 
              puts "is it trues?"
              puts t.is_participant?(u)
              @threads.push(format_thread(t,u))
              @counter += 1
              break
              # break
            elsif @counter == @threads_l.length
              puts "is it is it"
            # if @counter == @threads_l.length
              @threads.push(format_thread(nil,u))
              @counter = 1
            end
            # end
            # break
            @counter += 1
          end
          @threads.push(format_thread(nil,u)) if !@threads_l.present?
        end
      else
        threads = current_user.mailbox.conversations
        found = false
        threads.each do |c|
          @threads.push(format_thread(c,c.participants.reject{|p| found = true if !found && p[:id] == current_user.id}.first))
        end
      end
      
    end

    def reply_thread
      message = params[:thread][:body]
      # puts params[:thread][:attachment].open
      @message = ""
      @participants = []
      if params[:class] != "User"
        thread = Mailboxer::Conversation.find(params[:id]);
        puts "asfdasf"
         #reply_to_conversation(conversation, reply_body, subject = nil, should_untrash = true, sanitize_text = true, attachment = nil)
        message = current_user.reply_to_conversation(thread,message,"",true,true,params[:thread][:attachment]).message
        sender = message.sender
        @participants.push(format_user(sender))
        @message = format_message(message)
        puts "message"
        puts @message.inspect
      else
        i = current_user.send_message(User.find(params[:id]),message,"nothing")
        puts i.inspect
      end
    end

    private
    def get_threads
      @threads_l = current_user.mailbox.conversations 
    end

    def get_thread
      if params[:class_] == "Mailboxer::Conversation"
        @conv = Mailboxer::Conversation.find(params[:id].to_i)
      elsif params[:class_] == "User"
        @conv = User.find(params[:user_id].to_i)
      end
    end
end