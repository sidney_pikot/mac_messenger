class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :created_rooms, class_name: "Chatroom"
  has_many :chatroom_users
  acts_as_messageable
  mount_uploader :image, AvatarUploader

  scope :filtered, -> (filter) {
    ransack({fullname_cont: filter})
  }


  def mailboxer_email(object)
    #Check if an email should be sent for that object
    #if true
    # return "define_email@on_your.model"
    #if false
    # return nil
    self.email
  end
  def mailboxer_name
    self.name
  end
end
