class Chatroom < ApplicationRecord
    belongs_to :creator, class_name: "User"
    has_many :members, class_name: "ChatroomUser"
    
end
