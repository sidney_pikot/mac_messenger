class ChatroomUser < ApplicationRecord
    belongs_to :chatroom
    belongs_to :user
    acts_as_messageable
end
