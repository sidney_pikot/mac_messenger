class CreateChatrooms < ActiveRecord::Migration[5.2]
  def change
    create_table :chatrooms do |t|
      t.string :name, default: ""
      t.integer :status, default: 0
      t.belongs_to :user
      t.timestamps
    end
  end
end
